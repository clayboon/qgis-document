.. _`การสร้างข้อมูล Shapefile (Digitizing)`:

8.	การสร้างข้อมูล Shapefile (Digitizing)
==========================================

ดิจิไทซ์ (Digitizing) หมายถึง การสร้างข้อมูลเวกเตอร์โดยการคัดลอกลายแผนที่ฐานต้นฉบับ เช่น ภาพถ่ายจากดาวเทียม ภาพถ่ายทางอากาศ แผนที่ภูมิประเทศ เป็นต้น มาสร้างเป็นข้อมูลเวกเตอร์  คือจุด , เส้น  และข้อมูลรูปพื้นที่ เช่น เส้นทางถนน อาคารต่าง ๆ การใช้ประโยชน์ที่ดิน ตำแหน่งที่ตั้งของสถานที่สำคัญ ๆ เป็นต้น

**วิธีการ**

1. เปิดชั้น Base map เลือกประเภท Base map เป็นแบบ OpenStreetMap	
2. กดปุ่ม Layer >> Create Layer  >> New Shapefile Layer
3. ที่หน้าต่าง New Shapefile Layer แสดงขึ้นมา

.. _figure_modeler:

.. figure:: img/8_1.png
   :align: center 

**การกำหนดค่า**

•	File name >> ชื่อไฟล์ที่ต้องการจัดเก็บข้อมูล LAB/CreateShpefile ตั้งชื่อ Point
•	File encoding  >> เลือกเป็น system
•	Geometry type >> ประเภทของข้อมูลให้เลือกเป็น Point
•	EPSG >> 32647-WGS 84/UTM zone 47 N 

4. การสร้างคอลัมน์ข้อมูล โดยการกดปุ่ม Add attribute list กำหนดให้สร้าง 2 Field คือ ID และ Name โดยกำหนดแต่ละคอลัมน์ดังนี้

•	ID เก็บเป็นแบบ Whole number ขนาด 10.0
•	Name เก็บแบบ Text data ขนาด 100
	
หลังจากกำหนดรายละเอียดต่าง ๆ  และกดปุ่ม OK 

**8.1 การสร้าง Feature ให้กับข้อมูลจุด**

**วิธีการ**

1. เลือกที่ชั้นข้อมูล Point คลิกที่ปุ่ม |logo8_2| Toggle Editing เลือกปุ่ม |logo8_3| Add Feature 

.. _figure_modeler:

.. figure:: img/8_4.png
   :align: center

2. คลิกเมาส์ ลงพื้นที่ที่ต้องการ

.. _figure_modeler:

.. figure:: img/8_5.png
   :align: center 

3. เพิ่มรายละเอียด Feature Attribute

.. figure:: img/8_6.png
   :align: center 

4. การเพิ่มข้อมูลจุดอื่น ๆ ให้ทำตามขั้นตอนที่ 2 และ 3

.. |logo8_2| image:: img/8_2_Toggle_Editing.png
    :width: 25pt
    :height: 25pt

.. |logo8_3| image:: img/8_3.png
    :width: 25pt
    :height: 25pt

**8.2 การสร้าง Feature ให้กับข้อมูลเส้น**

**วิธีการ**

1. เลือกที่ชั้นข้อมูล Line คลิกที่ปุ่ม  |logo8_2|  Toggle Editing เลือกปุ่ม  |logo8_7|   Add Feature 

.. figure:: img/8_8.png
   :align: center 
 
2. คลิกเมาส์วาดเส้น ลงพื้นที่ที่ต้องการเมื่อวาดถึงจุดสุดท้ายของเส้นให้คลิกขวาหนึ่งครั้งเพื่อเสร็จสุดการวาดเส้นนั้น ๆ

.. figure:: img/8_9.png
   :align: center 
 
3. เพิ่มรายละเอียด Feature Attribute

.. figure:: img/8_10.png
   :align: center 
 
4. การเพิ่มข้อมูลเส้นอื่น ๆ เพิ่ม ให้ทำตามขั้นตอนที่ 2 และ 3

.. |logo8_7| image:: img/8_7.png
    :width: 25pt
    :height: 25pt
 
**8.3	การสร้าง Feature ให้กับข้อรูปพื้นที่**

**วิธีการ**

1. เลือกที่ชั้นข้อมูล Polygon คลิกที่ปุ่ม  |logo8_2|  Toggle Editing เลือกปุ่ม   |logo8_11|  Add Feature 

.. figure:: img/8_12.png
   :align: center  
 
2. คลิกเมาส์วาดรูปพื้นที่ ลงพื้นที่ที่ต้องการเมื่อวาดถึงจุดสุดท้ายของพื้นที่ให้คลิกขวาหนึ่งครั้งเพื่อเสร็จสุดการวาดรูปพื้นที่นั้น ๆ

.. figure:: img/8_13.png
   :align: center 

3. เพิ่มรายละเอียด Feature Attribute

.. figure:: img/8_14.png
   :align: center 

4. การเพิ่มข้อมูลเส้นอื่นเพิ่ม ให้ทำตามขั้นตอนที่ 2 และ 3

.. |logo8_11| image:: img/8_11.png
    :width: 25pt
    :height: 25pt

**Note:** คำสั่งสำคัญสำหรับการสร้างข้อมูล Shapefile (Digitizing)

1. ถ้าต้องการเคลื่อนย้าย Feature ที่วาดลงบนแผนที่ให้ใช้คำสั่ง Move Feature |logo8_15|
2. ถ้าต้องการลบหรือเพิ่มรวมถึงเคลื่อนย้าย Node ของ Feature ใช้คำสั่ง Node Tool  |logo8_16|
3. กำหนดการวาด Feature ให้แนบติดกัน Snapping ให้ใช้คำสั่ง Snapping Options โดยการ

**กดปุ่ม Settings >> Snapping Options**

.. figure:: img/8_17.png
   :align: center 

.. |logo8_15| image:: img/8_15.png
    :width: 25pt
    :height: 25pt 

.. |logo8_16| image:: img/8_16.png
    :width: 25pt
    :height: 25pt 

**การเปิดข้อมูลตารางข้อมูล (Open Attribute)**
	
**วิธีการ**

1. คลิกขวาที่ชั้นข้อมูล เลือก Properties
2. เลือกคำสั่ง Open Attribute

.. figure:: img/8_18.png
   :align: center
