.. _`การตั้งค่า Default Style`:

6.	การตั้งค่า Default Style
=====================

**วิธีการ**

1. คลิกขวาที่ชั้นข้อมูล BKK_branch ที่ปรับเลือก Style แล้ว >> Properties

2. เลือกคำสั่ง Symbology >> Style >> Save Default Style

.. _figure_modeler:

.. figure:: img/6_1.png
   :align: center


ชั้นข้อมูลที่ Save Default Style
 
.. _figure_modeler:

.. figure:: img/6_2.png
   :align: center

**Note:** ถ้าต้องการยกเลิก Save Default Style ให้กดเลือกที่ Restore Default Style
