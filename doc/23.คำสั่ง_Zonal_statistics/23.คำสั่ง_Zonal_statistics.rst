.. _`การนับจำนวนข้อมูลสาขา/ตู้ ATM ในพื้นที่ที่มีความหนาแน่นสูงและต่ำ (คำสั่ง Zonal statistics)`:

23. การนับจำนวนข้อมูลสาขา/ตู้ ATM ในพื้นที่ที่มีความหนาแน่นสูงและต่ำ (คำสั่ง Zonal statistics)
==================================================================================

**วิธีทำ**

1. เปิดชั้นข้อมูล BKK_ATM จากโฟลเดอร์ Lab\DataForKTB\UTM และชั้นข้อมูล Density_BKK_ATM จากโฟลเดอร์ Lab\Density

2. สร้าง Buffer 1 เมตร ให้กับชั้นข้อมูล BKK_ATM กำหนดให้เก็บข้อมูลที่ Lab/DataForKTB/UTM ตั้งชื่อไฟล์ BKK_ATM_1m

3. เลือกคำสั่ง Processing >> Processing Toolbox >> Raster analysis used >> Zonal statistics

4. Band คือช่องสัญญาณของข้อมูลที่นำมาวิเคราห์ กำหนดให้ใช้ Band 1

5. output column prefix คือคอลัมน์ ผลลัพธ์ของข้อมูล กำหนดให้ตั้งชื่อ Value

6. Statistics to calculate ค่าทางคณิตศาสตร์ที่คำนวณ

.. image:: img/23_1.png
 
**ผลลัพธ์ Zonal statistics**

.. image:: img/23_2.png

