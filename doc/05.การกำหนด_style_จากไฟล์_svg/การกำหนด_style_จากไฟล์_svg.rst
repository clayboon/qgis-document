.. _`การกำหนด Style จากไฟล์ SVG.`:

5. การกำหนด Style จากไฟล์ SVG
==============================
**วิธีการ**

1. คลิกขวาที่ชั้นข้อมูล BKK_branch >> Properties
2. เลือกคำสั่ง Symbology >> Style >> Symbol layer type >> SVG.
3. เลือกไฟล์ SVG จากโฟลเดอร์ Lab\SVG

.. image:: img/1.png
    :align: center
    :alt: alternate text    
 
**ผลลัพธ์**

.. image:: img/2.png
    :align: center
    :alt: alternate text
